package gupta.piyush.hiittimer;

import junit.framework.Assert;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class DRY {

	public static String str(int x) {
		return Integer.toString(x);
	}

	public static void log(String message) {
		Log.d("DEBUG", message);
	}

	public static void log(int x) {
		Log.d("DEBUG", str(x));
	}

	public static void log(String message, int x) {
		Log.d("DEBUG", message + x);
	}

	public static void log(String message, long x) {
		Log.d("DEBUG", message + x);
	}

	public static void log(long x) {
		Log.d("DEBUG", Long.toString(x));
	}

	public static void log(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			log(arr[i]);
		}
	}

	public static void assertMe(boolean b) {
		Assert.assertTrue(b);
	}

	public static void toaster(Context c, String message) {
		Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
	}

	public static String secondsToMS(int seconds) {
		int minutes = seconds / 60;
		int secondsLeft = seconds % 60;
		return timeToString(0, minutes, secondsLeft);
	}

	public static String timeToString(int hours, int minutes, int seconds) {
		return (minutes < 10 ? "0" : "") + minutes + ":"
				+ (seconds < 10 ? "0" : "") + seconds;
	}

	public static String millisecondsToTime(long time) {
		int seconds = (int) (time / 1000);
		int minutes = seconds / 60;
		int secondsLeft = seconds - (60 * minutes);
		return (minutes < 10 ? "0" : "") + minutes + ":"
				+ (secondsLeft < 10 ? "0" : "") + secondsLeft;
	}

	public static void toasterLong(Context c, String message) {
		Toast.makeText(c, message, Toast.LENGTH_LONG).show();

	}

}
