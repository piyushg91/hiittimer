package gupta.piyush.hiittimer;

import gupta.piyush.hiittimer.fragments.CircuitTimer;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class HowManyDialog extends DialogFragment implements
		OnEditorActionListener {

	public EditText mEditText;

	public HowManyDialog() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.how_many_dialog, container);
		mEditText = (EditText) view.findViewById(R.id.how_many_dialog_how_many);
		getDialog().setTitle("Number of repetitions?");
		mEditText.requestFocus();
		mEditText.setOnEditorActionListener(this);
		return view;
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {
			CircuitTimer ct = (CircuitTimer) ((MainFragmentActivity) getActivity())
					.getFragmentbyPosition(0);
			ct.mWorkoutParams.incrementResult(Integer.parseInt(mEditText
					.getText().toString()));
			mEditText.setText("");
			this.dismiss();
			return true;

		}
		return false;
	}

}
