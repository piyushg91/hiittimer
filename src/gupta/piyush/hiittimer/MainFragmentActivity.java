package gupta.piyush.hiittimer;

import gupta.piyush.hiittimer.db.DbCommon;
import gupta.piyush.hiittimer.db.Helper;
import gupta.piyush.hiittimer.db.ResultsDB;
import gupta.piyush.hiittimer.db.WorkoutListDB;

import java.util.ArrayList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONException;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.slidinglayer.SlidingLayer;

public class MainFragmentActivity extends FragmentActivity {
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private MyPagerAdapter adapter;
	public SlidingLayer sLayer;
	private WorkoutListDB wDB;
	private ResultsDB rDB;
	int[] colors = new int[] { Color.BLUE, Color.CYAN, Color.YELLOW, Color.RED,
			Color.GREEN, Color.DKGRAY, Color.MAGENTA };
	private LinearLayout workoutGraph;

	@Override
	protected void onCreate(Bundle savedBundleInstance) {
		super.onCreate(savedBundleInstance);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main_fragment);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		wDB = new WorkoutListDB(getApplicationContext());
		rDB = new ResultsDB(getApplicationContext());
		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setOffscreenPageLimit(2);
		adapter = new MyPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		tabs.setViewPager(pager);
		tabs.setIndicatorColor(0xFF30c7ff);
		sLayer = (SlidingLayer) findViewById(R.id.sliding_layer_workout_info);
		sLayer.setStickTo(SlidingLayer.STICK_TO_RIGHT);
		LayoutParams lrp = (LayoutParams) sLayer.getLayoutParams();
		sLayer.setLayoutParams(lrp);
		sLayer.setShadowWidth(0);
		sLayer.setShadowDrawable(null);
		sLayer.setOffsetWidth(0);
		workoutGraph = (LinearLayout) findViewById(R.id.workout_graph);
		firstTimeInfo();

	}

	public Fragment getFragmentbyPosition(int position) {
		return adapter
				.getFragment(pager, position, getSupportFragmentManager());
	}

	public void showSlidingLayer(long workoutListRowId) {
		setNonGraphViews(workoutListRowId);
		String[] exerciseNames = null;
		try {
			exerciseNames = wDB.getExercises(workoutListRowId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// Number of exercises, this will be the column
		int numOfExercises = exerciseNames.length;
		Cursor c = rDB.getResultsCursor(workoutListRowId);
		// Column Index for result
		int columnIndex = c.getColumnIndex(DbCommon.KEY_RESULTS);
		// Number of work outs, this will the number of rows
		int numOfWorkouts = c.getCount();
		int[][] resultsM = new int[numOfWorkouts][numOfExercises];
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
		mRenderer.setXAxisMin(0);
		mRenderer.setXAxisMax(numOfWorkouts + 1);
		mRenderer.setPanEnabled(false, false);
		String stringResults;
		int row = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			stringResults = c.getString(columnIndex);
			try {
				resultsM[row] = Helper.JSONToResults(exerciseNames,
						stringResults);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			row++;
		}
		for (int column = 0; column < numOfExercises; column++) {
			XYSeries series = new XYSeries(exerciseNames[column]);
			XYSeriesRenderer renderer = new XYSeriesRenderer();
			renderer.setColor(colors[column]);
			renderer.setPointStyle(PointStyle.CIRCLE);
			renderer.setLineWidth(3);
			for (row = 0; row < numOfWorkouts; row++) {
				series.add(row + 1, resultsM[row][column]);

			}
			dataset.addSeries(series);
			mRenderer.addSeriesRenderer(renderer);
		}
		workoutGraph.removeAllViews();
		workoutGraph.addView(ChartFactory.getLineChartView(
				getApplicationContext(), dataset, mRenderer));

		if (!sLayer.isOpened()) {
			sLayer.openLayer(true);
		}

	}

	private void setNonGraphViews(long workoutListRowId) {
		TextView onTime = (TextView) findViewById(R.id.workout_row_on_time);
		onTime.setText("On Time: "
				+ DRY.secondsToMS(wDB.getOnTime(workoutListRowId)));
		TextView offTime = (TextView) findViewById(R.id.workout_row_off_time);
		offTime.setText("Rest Time: "
				+ DRY.secondsToMS(wDB.getOffTime(workoutListRowId)));
		CheckBox trackStatus = (CheckBox) findViewById(R.id.workout_row_track_status);
		trackStatus.setChecked(wDB.getTrackStatus(workoutListRowId));
		trackStatus.setTag(workoutListRowId);
		trackStatus.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				long l = (Long) buttonView.getTag();
				wDB.updateTrackStatus(l, isChecked);

			}
		});
	}

	private void firstTimeInfo() {
		final String PREFS_NAME = "MyPrefsFile";
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		if (settings.getBoolean("my_first_time", true)) {
			List<String> one = new ArrayList<String>();
			one.add("Push Ups");
			one.add("Squats");
			one.add("Pull Ups");
			List<String> two = new ArrayList<String>();
			two.add("Jump Squats");
			two.add("Box Jumps");
			two.add("Jumping Jacks");
			WorkoutListDB db = new WorkoutListDB(this);
			db.addNewWorkout("Sample Workout 1", 60, 30, one, true);
			db.addNewWorkout("Sample Workout 2", 45, 15, two, true);
			ResultsDB rDB = new ResultsDB(this);
			rDB.addNewResult(1, 4, new int[] { 30, 45, 22 }, new String[] {
					"Push Ups", "Squats", "Pull Ups" });
			rDB.addNewResult(1, 5, new int[] { 35, 48, 25 }, new String[] {
					"Push Ups", "Squats", "Pull Ups" });
			rDB.addNewResult(1, 5, new int[] { 38, 48, 27 }, new String[] {
					"Push Ups", "Squats", "Pull Ups" });
			rDB.addNewResult(1, 6, new int[] { 42, 52, 30 }, new String[] {
					"Push Ups", "Squats", "Pull Ups" });
			rDB.addNewResult(1, 6, new int[] { 42, 54, 31 }, new String[] {
					"Push Ups", "Squats", "Pull Ups" });
			rDB.addNewResult(2, 4, new int[] { 15, 10, 48 }, new String[] {
					"Jump Squats", "Box Jumps", "Jumping Jacks" });
			rDB.addNewResult(2, 4, new int[] { 17, 12, 52 }, new String[] {
					"Jump Squats", "Box Jumps", "Jumping Jacks" });
			rDB.addNewResult(2, 4, new int[] { 14, 13, 56 }, new String[] {
					"Jump Squats", "Box Jumps", "Jumping Jacks" });
			rDB.addNewResult(2, 4, new int[] { 20, 15, 57 }, new String[] {
					"Jump Squats", "Box Jumps", "Jumping Jacks" });
			rDB.addNewResult(2, 4, new int[] { 22, 16, 60 }, new String[] {
					"Jump Squats", "Box Jumps", "Jumping Jacks" });
			settings.edit().putBoolean("my_first_time", false).commit();
		} else {
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			if (sLayer.isOpened()) {
				sLayer.closeLayer(true);
				return true;
			}

		default:
			return super.onKeyDown(keyCode, event);
		}
	}
}
