package gupta.piyush.hiittimer;

import gupta.piyush.hiittimer.fragments.CircuitTimer;
import gupta.piyush.hiittimer.fragments.NewWorkout;
import gupta.piyush.hiittimer.fragments.WorkoutList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class MyPagerAdapter extends FragmentPagerAdapter {
	private final String[] titles = { "Timer", "Workout List", "New Workout" };

	public MyPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getItemPosition(Object object) {
		return super.getItemPosition(object);
	}

	@Override
	public CharSequence getPageTitle(int pos) {
		return titles[pos];
	}

	@Override
	public Fragment getItem(int pos) {
		if (pos == 0) {
			return new CircuitTimer();
		} else if (pos == 1) {
			return new WorkoutList();
		} else if (pos == 2) {
			return new NewWorkout();
		}
		return null;
	}

	@Override
	public int getCount() {
		return titles.length;
	}

	public Fragment getFragment(ViewPager container, int position,
			FragmentManager fm) {
		String name = makeFragmentName(container.getId(), position);
		return fm.findFragmentByTag(name);
	}

	private String makeFragmentName(int viewId, int index) {
		return "android:switcher:" + viewId + ":" + index;
	}

}
