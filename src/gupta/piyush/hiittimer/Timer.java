package gupta.piyush.hiittimer;

import android.os.Handler;

public class Timer extends Thread {
	private Handler mHandler;
	private long onTime, offTime; // in milliseconds
	private int circuits;
	private long referenceTime;
	private long[] timeArray;
	private int tArrayIndex;
	private int rounds;
	private int numOfExercises;
	private long timeLeft;
	private long endReferenceTime;
	private WorkoutParams mmWorkoutParams;

	public int STARTED = 0;
	public int PAUSED = 1;
	public int FINISHED = 2;
	public int CURRENT_STATUS = FINISHED;

	public Timer(Handler mmhandler, WorkoutParams mWorkoutParams) {
		this.mHandler = mmhandler;
		this.mmWorkoutParams = mWorkoutParams;
	}

	public long getTimeLeft() {
		return timeArray[tArrayIndex];
	}

	public int getCurrentStatus() {
		return CURRENT_STATUS;
	}

	public void setParams() {
		circuits = mmWorkoutParams.circuits;
		onTime = (long) mmWorkoutParams.onTime * 1000l;
		offTime = (long) mmWorkoutParams.offTime * 1000l;
		numOfExercises = mmWorkoutParams.numOfExercises;
		rounds = this.circuits * numOfExercises * 2;
		timeArray = new long[rounds];
		for (int i = 0; i < rounds; i++) {
			timeArray[i] = this.onTime;
			timeArray[++i] = this.offTime;
		}
		tArrayIndex = 0;
	}

	private long getCurrentTime() {
		return System.currentTimeMillis();
	}

	private synchronized void nextRound() {
		if (tArrayIndex + 1 < rounds) {
			tArrayIndex += 1;
			mHandler.obtainMessage(WorkoutParams.UPDATE_EXER,
					timeArray[tArrayIndex]).sendToTarget();
		} else {
			CURRENT_STATUS = FINISHED;
			mHandler.obtainMessage(WorkoutParams.STATUS_FINISHED)
					.sendToTarget();
		}

	}

	@Override
	public void run() {
		CURRENT_STATUS = STARTED;

		while (CURRENT_STATUS != FINISHED) {
			if (CURRENT_STATUS == STARTED) {
				referenceTime = getCurrentTime();
				timeLeft = timeArray[tArrayIndex];
				endReferenceTime = timeLeft + referenceTime;
				while (getCurrentTime() <= endReferenceTime
						&& CURRENT_STATUS == STARTED) {
					timeArray[tArrayIndex] = endReferenceTime
							- getCurrentTime();
					mHandler.obtainMessage(WorkoutParams.UPDATE_TIME,
							timeArray[tArrayIndex]).sendToTarget();
					try {
						sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (CURRENT_STATUS == STARTED) {
					nextRound();
				}

			}

		}
		CURRENT_STATUS = FINISHED;

	}

}
