package gupta.piyush.hiittimer;

import gupta.piyush.hiittimer.db.ResultsDB;
import gupta.piyush.hiittimer.db.WorkoutListDB;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.json.JSONException;

import android.content.Context;


public class WorkoutParams {
	public int onTime;
	public int offTime;

	public String[] exerciseNames;
	public int numOfExercises;
	public int circuits = 3;
	public int currentCircuit;
	public int[] workoutResults;
	public int workoutResultIndex = -1;
	public List<String> workoutOrderList;
	public ListIterator<String> workoutOrderListIterator;
	public long workoutRowId;
	public boolean toTrack;

	public static final int UPDATE_TIME = 0;
	public static final int UPDATE_EXER = 1;
	public static final int STATUS_ON = 2;
	public static final int STATUS_OFF = 3;
	public static final int STATUS_FINISHED = 4;

	private WorkoutListDB wDB;
	private ResultsDB rDB;

	public WorkoutParams(Context context) {
		wDB = new WorkoutListDB(context);
		rDB = new ResultsDB(context);
	}

	public void setParams(long l) {
		workoutRowId = l;
		try {
			currentCircuit = 0;
			exerciseNames = wDB.getExercises(l);
			numOfExercises = exerciseNames.length;
			workoutResults = new int[numOfExercises];
			workoutOrderList = new ArrayList<String>();
			toTrack = wDB.getTrackStatus(l);
			for (int i = 0; i < numOfExercises; i++) {
				workoutOrderList.add(exerciseNames[i]);
				workoutOrderList.add("Rest");
			}
			resetWorkoutOrderListIterator();
			onTime = wDB.getOnTime(l);
			offTime = wDB.getOffTime(l);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void setCircuits(int x) {
		circuits = x;
	}

	public void resetWorkoutOrderListIterator() {
		workoutOrderListIterator = workoutOrderList.listIterator();
	}

	public void incrementResult(int a) {
		workoutResultIndex++;
		workoutResults[workoutResultIndex % numOfExercises] += a;
	}

	public void saveWorkoutResults() {
		rDB.addNewResult(workoutRowId, circuits, workoutResults, exerciseNames);

	}

}
