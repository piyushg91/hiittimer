package gupta.piyush.hiittimer.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DatabaseOpener extends SQLiteOpenHelper {
	private static DatabaseOpener mInstance = null;

	public static DatabaseOpener getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new DatabaseOpener(context);
		}
		return mInstance;
	}

	public DatabaseOpener(Context context) {
		super(context, DbCommon.DATABASE_NAME, null, DbCommon.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DbCommon.listSql);
		db.execSQL("PRAGMA foreign_keys = ON;");
		db.execSQL(DbCommon.resultSql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DbCommon.TABLE_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + DbCommon.TABLE_RESULTS);
		onCreate(db);
	}

}
