package gupta.piyush.hiittimer.db;

public class DbCommon {
	public static final String DATABASE_NAME = "HiitDatabase"; // Name of the
																// database
	public static final String TABLE_LIST = "workout_list"; // Name of the first
															// Table
	public static final String TABLE_RESULTS = "workout_results"; // Name of the
																	// second
																	// table
	public static final int DATABASE_VERSION = 1;

	// ROWS FOR THE FIRST TABLE
	public static final String KEY_LIST_ROWID = "_id";
	public static final String KEY_WORKOUTNAME = "workout_name";
	public static final String KEY_ONTIME = "on_time";
	public static final String KEY_OFFTIME = "off_time";
	public static final String KEY_EXERCISES = "exercises";
	public static final String KEY_TRACK_STATUS = "track_status";

	// Column Names for the first table
	public static String[] listColumns = { KEY_LIST_ROWID, KEY_WORKOUTNAME,
			KEY_ONTIME, KEY_OFFTIME, KEY_EXERCISES, KEY_TRACK_STATUS };

	//
	public static String listSql = "CREATE TABLE " + TABLE_LIST + " ("
			+ KEY_LIST_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ KEY_WORKOUTNAME + " TEXT NOT NULL, " + KEY_ONTIME
			+ " INTEGER NOT NULL, " + KEY_OFFTIME + " INTEGER NOT NULL, "
			+ KEY_EXERCISES + " TEXT NOT NULL, " + KEY_TRACK_STATUS
			+ " INTEGER NOT NULL" + ");";

	// Rows from second table
	public static final String KEY_RESULT_ROWID = "_id";
	public static final String KEY_LIST_ID = "list_id";
	public static final String KEY_CIRCUITS = "circuits";
	public static final String KEY_RESULTS = "results";

	public static String[] resultColumns = { KEY_RESULT_ROWID, KEY_LIST_ID,
			KEY_CIRCUITS, KEY_RESULTS };

	public static String resultSql = "CREATE TABLE " + TABLE_RESULTS + " ("
			+ KEY_RESULT_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ KEY_LIST_ID + " INTEGER NOT NULL, " + KEY_CIRCUITS
			+ " INTEGER NOT NULL, " + KEY_RESULTS + " TEXT NOT NULL, "
			+ " FOREIGN KEY (" + KEY_LIST_ID + ") REFERENCES " + TABLE_LIST
			+ " (" + KEY_LIST_ROWID + ")" + ");";
}
