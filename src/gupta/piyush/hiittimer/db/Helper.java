package gupta.piyush.hiittimer.db;

import gupta.piyush.hiittimer.DRY;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;


public class Helper {

	public static String createJSONStringList(List<String> exercises) {
		JSONObject jObject = new JSONObject();
		for (int i = 0; i < exercises.size(); i++) {
			try {
				jObject.accumulate(DRY.str(i), exercises.get(i));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return jObject.toString();
	}

	public static String[] getExercisesFromJSONList(String s)
			throws JSONException {
		JSONObject jObject = new JSONObject(s);
		int lengthJSON = jObject.length();
		String[] exercises = new String[lengthJSON];
		for (int i = 0; i < lengthJSON; i++) {
			exercises[i] = jObject.getString(DRY.str(i));

		}
		return exercises;

	}

	public static int booleanToInt(boolean status) {
		if (status) {
			return 1;
		} else {
			return 0;
		}
	}

	public static boolean intToBoolean(int x) {
		if (x == 1) {
			return true;
		} else {
			return false;
		}
	}

	public static String resultsToJSON(String[] exercises, int[] results) {
		JSONObject jObject = new JSONObject();
		for (int i = 0; i < exercises.length; i++) {
			try {
				jObject.accumulate(exercises[i], results[i]);

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return jObject.toString();
	}

	public static int[] JSONToResults(String[] exercises, String jSON)
			throws JSONException {
		JSONObject jObject = new JSONObject(jSON);
		int lengthJSON = jObject.length();
		int[] results = new int[lengthJSON];
		for (int i = 0; i < jObject.length(); i++) {
			results[i] = jObject.getInt(exercises[i]);
		}
		return results;
	}

}
