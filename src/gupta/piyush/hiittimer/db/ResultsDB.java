package gupta.piyush.hiittimer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ResultsDB {
	private SQLiteDatabase database;

	public ResultsDB(Context context) {
		DatabaseOpener mInstance = DatabaseOpener.getInstance(context);
		database = mInstance.getWritableDatabase();

	}

	public Cursor getCursor(long l) {
		return database.query(DbCommon.TABLE_RESULTS, DbCommon.resultColumns,
				DbCommon.KEY_RESULT_ROWID + "=" + l, null, null, null, null);
	}

	public Cursor getRegularCursor() {
		Cursor c = database.query(DbCommon.TABLE_RESULTS,
				DbCommon.resultColumns, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
			return c;
		}
		return null;
	}

	public Cursor getResultsCursor(long l) {
		Cursor c = database.query(DbCommon.TABLE_RESULTS,
				DbCommon.resultColumns, DbCommon.KEY_LIST_ID + "=" + l, null,
				null, null, null);
		if (c != null) {
			c.moveToFirst();
			return c;

		}
		return null;
	}

	public void addNewResult(long workoutRowId, int circuits, int[] results,
			String[] exercises) {
		ContentValues cv = new ContentValues();
		cv.put(DbCommon.KEY_LIST_ID, (int) workoutRowId);
		cv.put(DbCommon.KEY_CIRCUITS, (int) circuits);
		cv.put(DbCommon.KEY_RESULTS, Helper.resultsToJSON(exercises, results));
		database.insert(DbCommon.TABLE_RESULTS, null, cv);

	}
}
