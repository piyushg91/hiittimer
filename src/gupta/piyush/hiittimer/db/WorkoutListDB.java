package gupta.piyush.hiittimer.db;

import java.util.List;

import org.json.JSONException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class WorkoutListDB {
	private SQLiteDatabase database;

	public WorkoutListDB(Context context) {
		DatabaseOpener mInstance = DatabaseOpener.getInstance(context);
		database = mInstance.getWritableDatabase();

	}

	public Cursor getCursor(long l) {
		return database.query(DbCommon.TABLE_LIST, DbCommon.listColumns,
				DbCommon.KEY_LIST_ROWID + "=" + l, null, null, null, null);
	}

	public Cursor getRegularCursor() {
		Cursor c = database.query(DbCommon.TABLE_LIST, DbCommon.listColumns,
				null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
			return c;
		}
		return null;
	}

	public void addNewWorkout(String workoutName, int onTime, int offTime,
			List<String> exercises, boolean trackStatus) {
		ContentValues cv = new ContentValues();

		cv.put(DbCommon.KEY_WORKOUTNAME, workoutName);
		cv.put(DbCommon.KEY_ONTIME, onTime);
		cv.put(DbCommon.KEY_OFFTIME, offTime);
		cv.put(DbCommon.KEY_EXERCISES, Helper.createJSONStringList(exercises));
		cv.put(DbCommon.KEY_TRACK_STATUS, Helper.booleanToInt(trackStatus));
		database.insert(DbCommon.TABLE_LIST, null, cv);

	}

	public long[] getWorkoutListRowIds() {
		Cursor c = database.query(DbCommon.TABLE_LIST, DbCommon.listColumns,
				null, null, null, null, null);
		int iRowIndex = c.getColumnIndex(DbCommon.KEY_LIST_ROWID);
		long[] rowIds = new long[c.getCount()];
		int i = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			rowIds[i] = c.getLong(iRowIndex);
			i++;
		}
		return rowIds;
	}

	public CharSequence[] getWorkoutNames() {
		Cursor c = database.query(DbCommon.TABLE_LIST, DbCommon.listColumns,
				null, null, null, null, null);
		int iWorkoutIndex = c.getColumnIndex(DbCommon.KEY_WORKOUTNAME);
		CharSequence[] workoutNames = new CharSequence[c.getCount()];
		int i = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			workoutNames[i] = c.getString(iWorkoutIndex);
			i++;

		}
		return workoutNames;
	}

	public String getWorkoutName(long l) {
		Cursor c = getCursor(l);
		if (c != null) {
			c.moveToFirst();
			return c.getString(c.getColumnIndex(DbCommon.KEY_WORKOUTNAME));
		}
		return null;
	}

	public int getOnTime(long l) {
		Cursor c = getCursor(l);
		if (c != null) {
			c.moveToFirst();
			return c.getInt(c.getColumnIndex(DbCommon.KEY_ONTIME));
		}
		return (Integer) null;
	}

	public int getOffTime(long l) {
		Cursor c = getCursor(l);
		if (c != null) {
			c.moveToFirst();
			return c.getInt(c.getColumnIndex(DbCommon.KEY_OFFTIME));
		}
		return (Integer) null;
	}

	public String[] getExercises(long l) throws JSONException {
		Cursor c = getCursor(l);
		if (c != null) {
			c.moveToFirst();
			String s = c.getString(c.getColumnIndex(DbCommon.KEY_EXERCISES));
			return Helper.getExercisesFromJSONList(s);
		}
		return null;
	}

	public boolean getTrackStatus(long l) {
		Cursor c = getCursor(l);
		if (c != null) {
			c.moveToFirst();
			int trackStatus = c.getInt(c
					.getColumnIndex(DbCommon.KEY_TRACK_STATUS));
			return Helper.intToBoolean(trackStatus);
		}
		return false;
	}

	public void updateTrackStatus(long l, boolean trackStatus) {
		ContentValues cv = new ContentValues();
		cv.put(DbCommon.KEY_TRACK_STATUS, Helper.booleanToInt(trackStatus));
		database.update(DbCommon.TABLE_LIST, cv, DbCommon.KEY_LIST_ROWID + "="
				+ l, null);
	}

	public void delete(long l) {
		database.delete(DbCommon.TABLE_LIST,
				DbCommon.KEY_LIST_ROWID + "= " + l, null);
	}

}
