package gupta.piyush.hiittimer.fragments;

import gupta.piyush.hiittimer.DRY;
import gupta.piyush.hiittimer.HowManyDialog;
import gupta.piyush.hiittimer.Timer;
import gupta.piyush.hiittimer.WorkoutParams;
import gupta.piyush.hiittimer.db.WorkoutListDB;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment.NumberPickerDialogHandler;
import gupta.piyush.hiittimer.R;

import de.passsy.holocircularprogressbar.HoloCircularProgressBar;

public class CircuitTimer extends Fragment implements OnClickListener,
		NumberPickerDialogHandler {
	private View view;
	private TextView setWorkoutTV, timeLeftTV, setCircuits, circuitStatus,
			currentExercise, startStopBtn;
	private WorkoutListDB wDB;
	private Timer mTimer;
	private HowManyDialog howMany;
	private boolean mInForeground;
	public WorkoutParams mWorkoutParams;
	private LinearLayout holoProgressHolder;
	private HoloCircularProgressBar holoCircularProgressBar;
	private SoundPool changeNotification;
	private int BuzzerSound;

	final private int createView = 1;
	final private int setWorkout = 2;
	final private int startWorkout = 3;
	final private int pauseWorkout = 4;
	final private int unPauseWorkout = 5;
	final private int resetWorkout = 6;

	final private int animationStateStop = 1;
	final private int animationStatePlay = 2;
	final private int animationStatePause = 3;
	private int animationStateCurrent;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mInForeground = true;
		view = inflater.inflate(R.layout.fragment_activity_circuit_timer,
				container, false);
		howMany = new HowManyDialog();
		mWorkoutParams = new WorkoutParams(getActivity());
		mTimer = new Timer(mHandler, mWorkoutParams);
		wDB = new WorkoutListDB(getActivity());
		holoProgressHolder = (LinearLayout) view
				.findViewById(R.id.holo_progres_bar_holder);
		createProgressBarHolder(Color.GREEN, 0.0f);
		animationStateCurrent = animationStateStop;
		changeNotification = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

		BuzzerSound = changeNotification.load(getActivity(), R.raw.beep_7, 1);
		setViews(createView, null, -1);
		return view;
	}

	private void createProgressBarHolder(int color, float f) {
		holoProgressHolder.removeAllViews();
		holoCircularProgressBar = new HoloCircularProgressBar(getActivity(),
				color);
		holoCircularProgressBar.setProgress(f);
		holoProgressHolder.addView(holoCircularProgressBar);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.circuit_timer_set_workout:
			if (mTimer == null || mTimer.CURRENT_STATUS != mTimer.PAUSED) {
				final CharSequence[] workoutNames = wDB.getWorkoutNames();
				final long[] rowIds = wDB.getWorkoutListRowIds();
				if (rowIds.length == 0) {
					DRY.toaster(getActivity(), "No Workouts Found");
					break;
				}
				AlertDialog.Builder chooseWorkout = new AlertDialog.Builder(
						getActivity());
				chooseWorkout.setTitle("Choose a workout").setItems(
						workoutNames, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								setViews(setWorkout,
										(String) workoutNames[which],
										rowIds[which]);
								setmTimer();
							}
						});
				chooseWorkout.show();
			} else {
				setViews(resetWorkout, null, -1);
			}
			break;

		case R.id.circuit_timer_start_stop_btn:
			if (mTimer.CURRENT_STATUS == mTimer.FINISHED) {
				setViews(startWorkout, null, -1);
			} else if (mTimer.CURRENT_STATUS == mTimer.STARTED) {
				setViews(pauseWorkout, null, -1);
			} else if (mTimer.CURRENT_STATUS == mTimer.PAUSED) {
				setViews(unPauseWorkout, null, -1);
			}
			break;

		case R.id.circuit_timer_set_circuits:
			NumberPickerBuilder npb = new NumberPickerBuilder()
					.setFragmentManager(getChildFragmentManager())
					.setTargetFragment(CircuitTimer.this)
					.setStyleResId(R.style.BetterPickersDialogFragment);
			npb.show();

		default:
			break;
		}

	}

	private void setViews(int status, String name, long rowId) {
		switch (status) {
		case createView:
			// Time Left TextView
			timeLeftTV = (TextView) view
					.findViewById(R.id.circuit_timer_time_left);
			timeLeftTV.setVisibility(View.INVISIBLE);
			// Start Stop TextView
			startStopBtn = (TextView) view
					.findViewById(R.id.circuit_timer_start_stop_btn);
			startStopBtn.setVisibility(View.INVISIBLE);
			startStopBtn.setClickable(true);
			startStopBtn.setOnClickListener(this);

			// Workout Name
			setWorkoutTV = (TextView) view
					.findViewById(R.id.circuit_timer_set_workout);
			setWorkoutTV.setClickable(true);
			setWorkoutTV.setOnClickListener(this);
			// TextView for setting the number of circuits
			setCircuits = (TextView) view
					.findViewById(R.id.circuit_timer_set_circuits);
			setCircuits.setVisibility(View.INVISIBLE);
			setCircuits.setClickable(true);
			setCircuits.setOnClickListener(this);
			// Textview for listing the current exercise
			currentExercise = (TextView) view
					.findViewById(R.id.circuit_timer_current_exercise);
			currentExercise.setVisibility(View.INVISIBLE);
			// Circuit Status
			circuitStatus = (TextView) view
					.findViewById(R.id.circuit_timer_circuit_status);
			circuitStatus.setVisibility(View.INVISIBLE);

			break;
		case startWorkout:
			startStopBtn.setText(R.string.pause_time);
			setCircuits.setVisibility(View.INVISIBLE);
			setWorkoutTV.setVisibility(View.INVISIBLE);
			currentExercise.setVisibility(View.VISIBLE);
			incrementCircuit();
			mTimer.start();
			animationStateCurrent = animationStatePlay;
			animateHelper();
			break;
		case setWorkout:
			mWorkoutParams = new WorkoutParams(getActivity());
			mWorkoutParams.setParams(rowId);
			mTimer = new Timer(mHandler, mWorkoutParams);
			timeLeftTV.setVisibility(View.VISIBLE);
			updateTime(mWorkoutParams.onTime * 1000);
			startStopBtn.setVisibility(View.VISIBLE);
			currentExercise.setVisibility(View.VISIBLE);
			setCurrentExercise();
			//
			circuitStatus.setVisibility(View.VISIBLE);
			circuitStatus.setText("1" + "/" + mWorkoutParams.circuits);
			setWorkoutTV.setVisibility(View.VISIBLE);
			setCircuits.setVisibility(View.VISIBLE);

			createProgressBarHolder(Color.GREEN, 0.0f);
			break;

		case pauseWorkout:
			startStopBtn.setText(R.string.start_time);
			mTimer.CURRENT_STATUS = mTimer.PAUSED;
			animationStateCurrent = animationStatePause;
			Animation anim = new AlphaAnimation(0.0f, 1.0f);
			anim.setDuration(500); // You can manage the time of the blink with
									// this parameter
			anim.setStartOffset(20);
			anim.setRepeatMode(Animation.REVERSE);
			anim.setRepeatCount(Animation.INFINITE);
			timeLeftTV.startAnimation(anim);
			setWorkoutTV.setVisibility(View.VISIBLE);
			setWorkoutTV.setText(R.string.cancel_workout);

			break;
		case unPauseWorkout:
			startStopBtn.setText(R.string.pause_time);
			mTimer.CURRENT_STATUS = mTimer.STARTED;
			timeLeftTV.clearAnimation();
			animationStateCurrent = animationStatePlay;
			setWorkoutTV.setVisibility(View.INVISIBLE);
			animateHelper();

			break;

		case resetWorkout:
			timeLeftTV.clearAnimation();
			startStopBtn.setVisibility(View.GONE);
			startStopBtn.setText("Start");
			timeLeftTV.setVisibility(View.GONE);
			currentExercise.setVisibility(View.GONE);
			setWorkoutTV.setVisibility(View.VISIBLE);
			setWorkoutTV.setText(R.string.set_workout);
			circuitStatus.setVisibility(View.GONE);
			mWorkoutParams = null;
			mTimer = null;
			DRY.toaster(getActivity(), "Workout Cancelled");
			animationStateCurrent = animationStateStop;
			createProgressBarHolder(Color.GREEN, 0.0f);
			break;

		default:
			break;
		}
	}

	private void updateTime(long s) {
		timeLeftTV.setText(DRY.millisecondsToTime(s));
	}

	private void updateProgressBarHolder() {
		if (mWorkoutParams.workoutOrderListIterator.nextIndex() % 2 == 0
				&& animationStateCurrent == animationStatePlay) {
			createProgressBarHolder(Color.RED, 0.0f);
			animateHelper();

		} else if (animationStateCurrent == animationStatePlay) {
			createProgressBarHolder(Color.GREEN, 0.0f);
			animateHelper();
		}
	}

	private void showOrHideDialog() {
		if (mWorkoutParams.workoutOrderListIterator.nextIndex() % 2 == 0
				&& mWorkoutParams.toTrack && mInForeground) {
			showHowManyDlog();
		} else {
			hideHowManyDlog();

		}
		updateProgressBarHolder();
	}

	private void showHowManyDlog() {
		howMany.show(getActivity().getSupportFragmentManager(), "how many");
	}

	private void hideHowManyDlog() {
		if (howMany.isVisible()) {
			howMany.dismiss();
		}

	}

	private void setCurrentExercise() {
		if (mWorkoutParams.workoutOrderListIterator.hasNext()) {
			currentExercise.setText(mWorkoutParams.workoutOrderListIterator
					.next());
			showOrHideDialog();

		} else {
			incrementCircuit();
			mWorkoutParams.resetWorkoutOrderListIterator();
			currentExercise.setText(mWorkoutParams.workoutOrderListIterator
					.next());
			showOrHideDialog();
		}
	}

	private void setmTimer() {
		mTimer.setParams();
	}

	private void incrementCircuit() {
		mWorkoutParams.currentCircuit++;
		circuitStatus.setText(mWorkoutParams.currentCircuit + "/"
				+ mWorkoutParams.circuits);
	}

	private void finishWorkout() {
		startStopBtn.setVisibility(View.GONE);
		startStopBtn.setText("Start");
		timeLeftTV.setVisibility(View.GONE);
		currentExercise.setVisibility(View.GONE);
		setWorkoutTV.setVisibility(View.VISIBLE);
		setWorkoutTV.setText("SELECT WORKOUT");
		circuitStatus.setVisibility(View.GONE);
		if (mWorkoutParams.toTrack) {
			mWorkoutParams.saveWorkoutResults();
			DRY.toaster(getActivity(),
					"Good job! Your results have been saved.");
		} else {
			DRY.toaster(getActivity(), "Workout Complete!");
		}
		animationStateCurrent = animationStateStop;

	}

	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case WorkoutParams.UPDATE_TIME:
				Long currentTime = (Long) msg.obj;
				updateTime(currentTime);
				break;

			case WorkoutParams.STATUS_FINISHED:
				finishWorkout();
				break;

			case WorkoutParams.UPDATE_EXER:
				changeNotification.play(BuzzerSound, 1, 1, 0, 0, 1);
				setCurrentExercise();

				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onStart() {
		super.onStart();
		mInForeground = true;
	}

	@Override
	public void onPause() {
		super.onPause();
		mInForeground = false;
		hideHowManyDlog();
	}

	@Override
	public void onResume() {
		super.onResume();
		mInForeground = true;
	}

	@Override
	public void onStop() {
		super.onStop();
		mInForeground = false;
		hideHowManyDlog();
	}

	@Override
	public void onDialogNumberSet(int reference, int number, double decimal,
			boolean isNegative, double fullNumber) {
		circuitStatus.setText(1 + "/" + number);
		mWorkoutParams.circuits = number;
		setmTimer();

	}

	private void animate(HoloCircularProgressBar pB, long duration,
			final AnimatorListener listener) {
		final float progress = 1.0f;
		final HoloCircularProgressBar progressBar = pB;
		final ObjectAnimator progressBarAnimator = ObjectAnimator.ofFloat(
				progressBar, "progress", progress);
		progressBarAnimator.setDuration(duration);

		progressBarAnimator.addListener(new AnimatorListener() {

			@Override
			public void onAnimationCancel(final Animator animation) {
			}

			@Override
			public void onAnimationEnd(final Animator animation) {
				progressBar.setProgress(progress);
			}

			@Override
			public void onAnimationRepeat(final Animator animation) {
			}

			@Override
			public void onAnimationStart(final Animator animation) {
			}
		});
		progressBarAnimator.addListener(listener);
		progressBarAnimator.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(final ValueAnimator animation) {
				if (animationStateCurrent == animationStatePlay) {
					progressBar.setProgress((Float) animation
							.getAnimatedValue());
				} else if (animationStateCurrent == animationStatePause) {
					createProgressBarHolder(progressBar.getProgressColor(),
							progressBar.getProgress());
					animation.cancel();
				}

			}
		});
		progressBar.setMarkerProgress(progress);
		progressBarAnimator.start();
	}

	private void animateHelper() {
		animate(holoCircularProgressBar, mTimer.getTimeLeft(),
				new AnimatorListener() {

					@Override
					public void onAnimationStart(Animator animation) {
					}

					@Override
					public void onAnimationRepeat(Animator animation) {
					}

					@Override
					public void onAnimationEnd(Animator animation) {
					}

					@Override
					public void onAnimationCancel(Animator animation) {
					}
				});
	}

}
