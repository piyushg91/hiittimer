package gupta.piyush.hiittimer.fragments;

import gupta.piyush.hiittimer.DRY;
import gupta.piyush.hiittimer.MainFragmentActivity;
import gupta.piyush.hiittimer.R;
import gupta.piyush.hiittimer.db.WorkoutListDB;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.doomonafireball.betterpickers.hmspicker.HmsPickerBuilder;
import com.doomonafireball.betterpickers.hmspicker.HmsPickerDialogFragment.HmsPickerDialogHandler;

public class NewWorkout extends Fragment implements OnSeekBarChangeListener,
		OnClickListener, HmsPickerDialogHandler {
	private View view;
	private SeekBar selectNumOfExercises;
	private TextView numOfExercises, onTimeTV, offTimeTV;
	private ListView editTextListView;
	private ViewHolder viewHolder;
	private NewWorkoutListViewAdapter adapter;
	private Button btnSetOn, btnSetOff, saveToDb;
	private EditText workoutName;
	private CheckBox trackStatus;
	private int onTime;
	private int offTime;
	private int onTimeReference = 1;
	private int offTimeReference = 2;
	private AlertDialog trackDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.fragment_activity_new_workout,
				container, false);
		viewHolder = new ViewHolder();
		setViews();
		makeTrackStatusAlertDialog();
		return view;
	}

	private void makeTrackStatusAlertDialog() {
		AlertDialog.Builder trackStatusDialog = new AlertDialog.Builder(
				getActivity());
		trackStatusDialog
				.setMessage(
						getResources().getString(R.string.track_status_dialog))
				.setCancelable(true)
				.setPositiveButton("Got it",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
		trackDialog = trackStatusDialog.create();

	}

	private void setListView() {
		editTextListView = (ListView) view
				.findViewById(R.id.edit_text_list_view);
		editTextListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				DRY.log(position);
			}
		});

		List<ListViewItem> items = new ArrayList<ListViewItem>();
		for (int i = 0; i < 7; i++) {
			final int j = i;
			items.add(new ListViewItem() {
				{
					exerciseName = new EditText(getActivity());
					caption = "Exercise " + DRY.str(j + 1);
				}
			});

		}
		adapter = new NewWorkoutListViewAdapter(getActivity(), items,
				viewHolder);
		editTextListView.setAdapter(adapter);
	}

	private void setButtons() {
		btnSetOn = (Button) view.findViewById(R.id.btn_set_off);
		btnSetOff = (Button) view.findViewById(R.id.btn_set_on);
		saveToDb = (Button) view.findViewById(R.id.save_to_db);
		btnSetOn.setOnClickListener(this);
		btnSetOff.setOnClickListener(this);
		saveToDb.setOnClickListener(this);
		// Corresponding TextViews
		onTimeTV = (TextView) view.findViewById(R.id.on_time);
		offTimeTV = (TextView) view.findViewById(R.id.off_time);
		onTime = setTime(onTimeTV, 0, 0, 45);
		offTime = setTime(offTimeTV, 0, 0, 15);
	}

	private void setSeekBar() {
		selectNumOfExercises = (SeekBar) view
				.findViewById(R.id.select_num_of_exercises);
		numOfExercises = (TextView) view.findViewById(R.id.num_of_exercises);
		selectNumOfExercises.setOnSeekBarChangeListener(this);
		selectNumOfExercises.setMax(7);
		selectNumOfExercises.setProgress(3);
		numOfExercises.setText(getResources().getString(
				R.string.select_number_of_workouts)
				+ "3");

	}

	private void setViews() {
		workoutName = (EditText) view.findViewById(R.id.workout_name);
		setListView();
		setButtons();
		setSeekBar();
		trackStatus = (CheckBox) view.findViewById(R.id.track_status);
		trackStatus.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					trackDialog.show();
				}

			}
		});

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		seekBar.setProgress(progress);
		numOfExercises.setText(getResources().getString(
				R.string.select_number_of_workouts)
				+ DRY.str(progress));
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		adapter.hideOrShow(seekBar.getProgress());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_set_on:
			HmsPickerBuilder hpb1 = new HmsPickerBuilder()
					.setStyleResId(R.style.BetterPickersDialogFragment)
					.setReference(onTimeReference)
					.setFragmentManager(getChildFragmentManager())
					.setTargetFragment(NewWorkout.this);
			hpb1.show();
			break;

		case R.id.btn_set_off:
			HmsPickerBuilder hpb2 = new HmsPickerBuilder()
					.setStyleResId(R.style.BetterPickersDialogFragment)
					.setReference(offTimeReference)
					.setFragmentManager(getChildFragmentManager())
					.setTargetFragment(NewWorkout.this);
			hpb2.show();
			break;

		case R.id.save_to_db:
			if (workoutName.getText().toString().equals("")) {
				DRY.toaster(getActivity(),
						"You must include a name for this workout");
				break;
			} else if (selectNumOfExercises.getProgress() < 1) {
				DRY.toaster(getActivity(), "You need at least one exercise");
				break;
			} else if (!viewHolder.checkForExerciseNames(selectNumOfExercises
					.getProgress())) {
				DRY.toaster(getActivity(), "Each exercise must have a name");
				break;
			}
			WorkoutListDB db = new WorkoutListDB(getActivity());
			db.addNewWorkout(workoutName.getText().toString(), onTime, offTime,
					viewHolder.getExerciseNames(adapter.hideOrShow),
					trackStatus.isChecked());
			viewHolder.resetEditTexts();
			workoutName.setText("");
			WorkoutList wl = (WorkoutList) ((MainFragmentActivity) getActivity())
					.getFragmentbyPosition(1);
			wl.update();
			DRY.toaster(getActivity(), "Workout Added");
			break;
		default:
			break;
		}
	}

	public int setTime(TextView tv, int hours, int minutes, int seconds) {
		String time = DRY.timeToString(hours, minutes, seconds);
		tv.setText(time);
		return convertToSeconds(hours, minutes, seconds);
	}

	public int convertToSeconds(int hours, int minutes, int seconds) {
		return 3600 * hours + 60 * minutes + seconds;
	}

	@Override
	public void onDialogHmsSet(int reference, int hours, int minutes,
			int seconds) {
		if (convertToSeconds(hours, minutes, seconds) > 3599) {
			DRY.toaster(getActivity(), "Total Time must be less than one hour");
			return;
		}
		if (reference == onTimeReference) {
			onTime = setTime(onTimeTV, hours, minutes, seconds);
		} else if (reference == offTimeReference) {
			offTime = setTime(offTimeTV, hours, minutes, seconds);
		}
	}

}
