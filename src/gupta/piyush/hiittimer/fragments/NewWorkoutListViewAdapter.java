package gupta.piyush.hiittimer.fragments;

import gupta.piyush.hiittimer.R;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

public class NewWorkoutListViewAdapter extends BaseAdapter {
	LayoutInflater inflater;
	List<ListViewItem> items;
	ViewHolder viewHolder;
	int hideOrShow = 3;

	public NewWorkoutListViewAdapter(Activity context,
			List<ListViewItem> items, ViewHolder viewHolder) {
		super();
		this.items = items;
		this.inflater = context.getLayoutInflater();
		this.viewHolder = viewHolder;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListViewItem item = items.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.exercise_row, null);
			EditText et = (EditText) convertView.findViewById(R.id.exercise);
			et.setHint(item.caption);
			et.setId(position);
			et.setSelectAllOnFocus(true);
			viewHolder.addEditText(et);
			if (position < hideOrShow) {
				et.setVisibility(View.VISIBLE);
			} else {
				et.setVisibility(View.GONE);
			}
		}
		return convertView;

	}
	
	public void hideOrShow(int x) {
		hideOrShow = x;
		resetData();
	}
	
	public void resetData(){
		for (int i = 0; i < 7; i++) {
			EditText et = viewHolder.getEditText(i);
			if (i < hideOrShow) {
				et.setVisibility(View.VISIBLE);
			} else {
				et.setVisibility(View.GONE);
			}
		}
	}

	public List<ListViewItem> getItems() {
		return items;
	}

}
