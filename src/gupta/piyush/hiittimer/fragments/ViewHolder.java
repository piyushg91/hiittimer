package gupta.piyush.hiittimer.fragments;

import java.util.ArrayList;
import java.util.List;

import android.widget.EditText;

public class ViewHolder {
	private List<EditText> notes = new ArrayList<EditText>();

	public void addEditText(EditText et) {
		notes.add(et);
	}

	public EditText getEditText(int position) {
		return notes.get(position);
	}

	public List<String> getExerciseNames(int howMany) {
		List<String> toReturn = new ArrayList<String>();
		for (int i = 0; i < howMany; i++) {
			toReturn.add(notes.get(i).getText().toString());
		}
		return toReturn;
	}

	public boolean checkForExerciseNames(int howMany) {
		for (int i = 0; i < howMany; i++) {
			if (notes.get(i).getText().toString().equals("")) {
				return false;
			}
		}
		return true;
	}

	public void resetEditTexts() {
		for (int i = 0; i < notes.size(); i++) {
			notes.get(i).setText("");
		}
	}

}
