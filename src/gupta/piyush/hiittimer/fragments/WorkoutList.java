package gupta.piyush.hiittimer.fragments;

import gupta.piyush.hiittimer.R;
import gupta.piyush.hiittimer.db.WorkoutListDB;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class WorkoutList extends Fragment {
	private View view;
	private ListView workoutListView;
	private WorkoutListListViewAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_activity_workout_list,
				container, false);
		setListView();
		return view;
	}

	public void setListView() {
		workoutListView = (ListView) view.findViewById(R.id.workout_list_view);
		WorkoutListDB wDB = new WorkoutListDB(getActivity());
		adapter = new WorkoutListListViewAdapter(getActivity(), wDB,
				wDB.getRegularCursor());
		workoutListView.setAdapter(adapter);
		workoutListView.setDivider(getResources().getDrawable(R.color.blue));

	}

	public void update() {
		adapter.update();
	}

}
