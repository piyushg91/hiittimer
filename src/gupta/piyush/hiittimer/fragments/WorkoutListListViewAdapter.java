package gupta.piyush.hiittimer.fragments;

import gupta.piyush.hiittimer.MainFragmentActivity;
import gupta.piyush.hiittimer.R;
import gupta.piyush.hiittimer.db.DbCommon;
import gupta.piyush.hiittimer.db.WorkoutListDB;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class WorkoutListListViewAdapter extends CursorAdapter {
	LayoutInflater inflater;
	Context context;
	Cursor cursor;
	WorkoutListDB wDB;

	@SuppressWarnings("deprecation")
	public WorkoutListListViewAdapter(Context context, WorkoutListDB wDB,
			Cursor cursor) {
		super(context, cursor);
		this.cursor = cursor;
		this.context = context;
		this.wDB = wDB;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public void bindView(View view, Context arg1, Cursor c) {
		TextView workoutName = (TextView) view.findViewById(R.id.workout);
		workoutName.setText(c.getString(c
				.getColumnIndex(DbCommon.KEY_WORKOUTNAME)));
		view.setClickable(true);
		workoutName.setTextSize(25);
		workoutName.setClickable(true);
		final long exerciseRowId = c.getLong(c
				.getColumnIndex(DbCommon.KEY_LIST_ROWID));
		final String name = c.getString(c
				.getColumnIndex(DbCommon.KEY_WORKOUTNAME));
		workoutName.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				AlertDialog.Builder toDelete = new AlertDialog.Builder(context);
				toDelete.setTitle("Do you want to delete \"" + name
						+ " \" and its results?");
				toDelete.setPositiveButton("Delete",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								wDB.delete(exerciseRowId);
								update();

							}
						});
				toDelete.setNegativeButton("No", null);
				AlertDialog toDeleteDialog = toDelete.create();
				toDeleteDialog.show();
				return true;
			}
		});
		ImageView showStats = (ImageView) view.findViewById(R.id.arrow);
		showStats.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MainFragmentActivity mFA = (MainFragmentActivity) (((MainFragmentActivity) context));
				mFA.showSlidingLayer(exerciseRowId);

			}
		});
		view.setId((int) c.getLong(c.getColumnIndex(DbCommon.KEY_LIST_ROWID)));
	}

	@Override
	public View newView(Context arg0, Cursor c, ViewGroup arg2) {
		View view = inflater.inflate(R.layout.workout_row, arg2, false);
		return view;
	}

	public void update() {
		cursor.close();
		Cursor newC = wDB.getRegularCursor();
		cursor = swapCursor(newC);
		notifyDataSetChanged();
	}

}
